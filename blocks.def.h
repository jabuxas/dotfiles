//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"  ", "ncmpcpp --current-song", 1, 0},

	{"  ", "~/Docs/Scripts/Dwmblocks/cpu-dwm", 5, 0},

	{"  ", "~/Docs/Scripts/Dwmblocks/ram-dwm",	5,		0},

	{"  ", "~/Docs/Scripts/Dwmblocks/volumedwm", 1, 0},

	{"  ", "date '+%d %b %Y, %a %H:%M '",					30,		0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
